class LeaderboardList extends HTMLElement {
  static get observedAttributes() {
    return ["id", "rank", "name", "score", "fail", "background-color", "crown"];
  }

  constructor() {
    super();
    this._root = this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.update();
  }

  update() {
    this._root.innerHTML = `
      <div class="rank">
        ${this.crown()}
        ${this.getAttribute("rank")}
      </div>

      <div class="name">
        ${this.getAttribute("name")}
      </div>

      <div class="score">
        ${this.getAttribute("score")}
        <span style="font-size: 14px">Score</span>
      </div>

      <div class="fail">
        ${this.getAttribute("fail")}
        <span style="font-size: 14px">Fail${
          parseInt(this.getAttribute("fail")) > 1 ? "s" : ""
        }</span>
      </div>

      <style>
        * {
          box-sizing: border-box !important;
        }

        :host {
          flex-grow: 1;

          position: relative;
          display: flex;
          align-items: center;

          width: 100%;
          height: 100%;
          
          font-size: 30px;
          font-weight: bold;

          padding: 16px;

          background-color: ${this.getAttribute("background-color")};
          overflow: hidden;
        }

        .rank {
          display: flex;
          justify-content: center;
          gap: 16px;
          align-items: center;
          width: 200px !important;
          text-align: center;
        }

        .rank img {
          width: 50px;
          height: auto;
        }

        .name {
          flex-grow: 1;
        }

        .score, .fail {
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          gap: 5px;
          width: 100px !important;
          height: 100px !important;
          background-color: #ffffffbf;
          border-radius: 100%; 
          box-shadow: 0 0 10px black inset;
        }

        .score {
          margin-right: 32px;
          color: green;
        }

        .fail {
          color: red;
        }
      </style>
    `;
  }

  crown() {
    if (this.getAttribute("crown") == 1) {
      return `
        <img src="./images/crown.png">
      `;
    } else {
      return ``;
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    this.update();
  }
}

customElements.define("leaderboard-list", LeaderboardList);
