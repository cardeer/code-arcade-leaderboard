const leaderboard = document.querySelector(".leaderboard");

async function getData() {
  const {
    data,
  } = await axios.post(
    "http://www.comsci.science.kmitl.ac.th:5000/teamcompetition/leaderboard/final",
    { comp_id: 2 }
  );

  // console.log(data);
  return data
    .filter((e) => e.team_id != 36)
    .sort((a, b) => a.team_rank - b.team_rank);
}

async function init() {
  const data = await getData();
  const length = data.length - 1;

  leaderboard.innerHTML = "";

  data.forEach((e, i) => {
    const list = document.createElement("leaderboard-list");
    list.setAttribute("id", e.team_id);
    list.setAttribute("rank", e.team_rank);
    list.setAttribute("name", e.team_name);
    list.setAttribute("score", e.team_score);
    list.setAttribute("fail", e.team_fail);
    list.setAttribute(
      "background-color",
      `rgba(255, 165, 0, ${(length - i) / length})`
    );
    list.setAttribute("crown", i == 0 ? "1" : "0");
    leaderboard.appendChild(list);
  });
}

init();

setInterval(function () {
  init();
}, 120 * 1000);
